Rails.application.routes.draw do
  root 'persons#index'
  get 'persons/index'
  get 'persons/new'
  get 'employees/index'
  get 'employees/show'
  get 'employees/new'

  post 'persons/new', to: 'persons#create' 
end
