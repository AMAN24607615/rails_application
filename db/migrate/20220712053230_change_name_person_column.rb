class ChangeNamePersonColumn < ActiveRecord::Migration[5.2]
  def change
    rename_column :people, :secondName, :lastName
  end
end
