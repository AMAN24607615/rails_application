class CreateEmployees < ActiveRecord::Migration[5.2]
  def change
    create_table :employees do |t|
      t.string:firstName
      t.string:secondName
      t.string:email
      t.string:phone
      t.integer:empId
      t.string:gender
      t.string:address
      t.date:dob
      t.date:doj
      t.timestamps
    end
  end
end
