class RenameColumnName < ActiveRecord::Migration[5.2]
  def change
    rename_column :employees, :secondName, :lastName
  end
end
