class CreatePeople < ActiveRecord::Migration[5.2]
  def change
    create_table :people do |t|
      t.string:firstName
      t.string:secondName
      t.string:email
      t.string:phone
      t.string:gender
      t.string:address
      t.date:dob
      t.timestamps
    end
  end
end
