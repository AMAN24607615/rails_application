class CreateBooks < ActiveRecord::Migration[5.2]
  def change
    create_table :books do |t|
      t.date:publish_at
      t.timestamps
    end
  end
end
