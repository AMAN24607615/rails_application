class CreateDepartments < ActiveRecord::Migration[5.2]
  def change
    create_table :departments do |t|
      t.string:dname
      t.string:dnumber
      t.string:location
      t.timestamps
    end
  end
end
