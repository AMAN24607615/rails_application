class PersonsController < ApplicationController
  def index
    @persons = Person.all
  end

  def new
    @person = Person.new(params[:id])
  end

  def create
    @person = Person.new(persons_params)
    flash[:success] = "Comment successfully created!"
    if @person.save
      redirect_to persons_index_url
    else
      flash[:warning] = "Something went wrong, try again. If problem persists please let our team know about it!"
      render persons_new_path
    end
  end

  private
    def persons_params
      params.require(:person).permit(:firstName, :lastName, :email, :phone, :gender,:address,:dob)
    end
end
