class Person < ApplicationRecord
    validates :firstName,presence: true
    validates :lastName,presence: true
    validates :terms_of_service, acceptance:  { message: 'must be accepted' }
    validates :email, confirmation: true
    validates :email_confirmation, presence: true
end
